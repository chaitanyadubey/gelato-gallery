package com.chaitanyadubey.gelatogallery.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;



@SuppressLint("AppCompatCustomView")
public class DynamicHeightNetworkImageView extends ImageView {
    private double mAspectRatio = 1.0;
    String TAG="B=B "+getClass();

    public DynamicHeightNetworkImageView(Context context) {
        super(context);
    }

    public DynamicHeightNetworkImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicHeightNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setAspectRatio(double aspectRatio) {
        Log.d(TAG,"setAspectRatio aspectRatio="+aspectRatio);
        mAspectRatio = aspectRatio;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(TAG,"onMeasure.start");
        int measuredWidth = getMeasuredWidth();
        Log.d(TAG,"onMeasure measuredWidth="+measuredWidth);
        setMeasuredDimension(measuredWidth, (int) (measuredWidth / mAspectRatio));
    }
}
