package com.chaitanyadubey.gelatogallery.entity;

import android.util.Log;

import com.chaitanyadubey.gelatogallery.helper.Utils;

public class Image {

    String TAG="B=B"+this.getClass();

    public String id;
    public String author;
    public int width;
    public int height;
    public String url;
    public String download_url;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDownload_url() {
        return download_url;
    }

    public void setDownload_url(String download_url) {
        this.download_url = download_url;
    }


    @Override
    public String toString() {
        return "Image{" +
                "id='" + id + '\'' +
                ", author='" + author + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", url='" + url + '\'' +
                ", ThumbnailImageUrl='" + getThumbnailImageUrl() + '\'' +

                ", download_url='" + download_url + '\'' +
                '}';
    }


    public String getCustomImageURL(int customWidth)
    {
        int customHeight = customWidth;

        try {
            customHeight = (int)( customWidth / getAspectRatio() );

            Log.d(TAG,"getCustomImageURL aspectRatio width ="+width);
            Log.d(TAG,"getCustomImageURL aspectRatio height ="+height);
            Log.d(TAG,"getCustomImageURL aspectRatio customHeight ="+customHeight);
            Log.d(TAG,"getCustomImageURL aspectRatio customWidth ="+customWidth);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            Log.d(TAG,"getCustomImageURL aspectRatio exception "+e.getMessage());
        }

        String w_h="/"+customWidth+"/"+customHeight;
        Log.d(TAG,"getCustomImageURL aspectRatio w_h ="+w_h);

        String imageUrl =  "https://picsum.photos/id/"+id+w_h;
        Log.d(TAG,"getCustomImageURL="+imageUrl);

        return imageUrl;
    }

    public String getThumbnailImageUrl(){
        return getCustomImageURL(256);
    }

    public String getFullScreenImageUrl(){

        int maxWidth = 1024;

        double aspectRatio = getAspectRatio();
        if(aspectRatio < 1.0)
        {
          /* CASE : Height is more than the width */
            int maxHeight = 1024;
            maxWidth = (int) (maxHeight * aspectRatio);
        }

        return getCustomImageURL(maxWidth);
    }


    public double getAspectRatio(){
        return  (double)this.width / (double)height;
    }
}


