package com.chaitanyadubey.gelatogallery.activity;

/*
*
* Created by Chaitanya Dubey
* Dated Feb 15, 2021
* This is SplashActivity is loading screen
*
* */

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import com.chaitanyadubey.gelatogallery.R;

public class SplashActivity extends AppCompatActivity {
    int SPLASH_DISPLAY_DURATION = 500;
    String TAG = "B=B " + getClass();

    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity_layout);
        Log.d(TAG,"onCreate.Start");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);




        mHandler=new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this, DashboardActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
                finish();


            }
        }, SPLASH_DISPLAY_DURATION);

        Log.d(TAG,"onCreate.End");
    }
}