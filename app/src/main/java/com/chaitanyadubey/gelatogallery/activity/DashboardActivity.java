
package com.chaitanyadubey.gelatogallery.activity;



/*
 *
 * Created by Chaitanya Dubey
 * Dated Feb 15, 2021
 * This is DashboardActivity is home screen
 *
 * */

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.chaitanyadubey.gelatogallery.R;
import com.chaitanyadubey.gelatogallery.entity.Image;
import com.chaitanyadubey.gelatogallery.helper.ProgressBarHelper;

import com.chaitanyadubey.gelatogallery.helper.Utils;
import com.chaitanyadubey.gelatogallery.listener.RecyclerViewScrollListener;
import com.chaitanyadubey.gelatogallery.rest.RestClient;
import com.chaitanyadubey.gelatogallery.rest.RestInterface;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {
    String TAG = "B=B " + getClass();

    private RestClient mRestClient;
    private RestInterface mRestInterface;
    int mPage = 1;
    int mLimit = 30;
    int mPicksRecordsTotal = 40;
    ArrayList<Image> mImageListResponse;
    private ProgressBarHelper mProgressBarHelper;
    RecyclerView recyclerView_images;
    ImageRecyclerViewAdapter mImageRecyclerViewAdapter;
    StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private int mSpanCount = 2;

    DrawerLayout mDrawerLayoutDashboard;
    ActionBarDrawerToggle mActionBarDrawerToggle;

    NavigationView mNavigationViewDashboard;
    private String mNavigationBarColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate.Start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity_main_layout);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            loadSideNavigation();
        }
        mNavigationBarColor =  String.format("#%06X", (0xFFFFFF & getWindow().getNavigationBarColor()));

        mRestClient = new RestClient(this);
        mRestInterface = mRestClient.getApiService();

        mProgressBarHelper = new ProgressBarHelper(this);
        Log.d(TAG, "onCreate mRestClient=" + mRestClient);
        Log.d(TAG, "onCreate mRestInterface=" + mRestInterface);
        Log.d(TAG, "onCreate mProgressBarHelper=" + mProgressBarHelper);
        mImageListResponse = new ArrayList<Image>();


        mImageRecyclerViewAdapter = new ImageRecyclerViewAdapter(this);


        recyclerView_images = (RecyclerView) findViewById(R.id.recyclerView_images);

        if ( getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
            mSpanCount = 4;
        } else {
            mSpanCount = 2;
        }
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(mSpanCount, StaggeredGridLayoutManager.VERTICAL);

        if (mImageRecyclerViewAdapter != null && recyclerView_images != null) {
            if (mStaggeredGridLayoutManager != null) {
                recyclerView_images.setLayoutManager(mStaggeredGridLayoutManager);
                recyclerView_images.setItemAnimator(new DefaultItemAnimator());

                RecyclerViewScrollListener recyclerViewScrollListener = new RecyclerViewScrollListener(mStaggeredGridLayoutManager) {

                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        // Triggered only when new data needs to be appended to the list
                        // Add whatever code is needed to append new items to the bottom of the list
                        Log.d(TAG, "onLoadMore.START");
                        Log.d(TAG, "onLoadMore page=" + page);
                        Log.d(TAG, "onLoadMore totalItemsCount=" + totalItemsCount);
                        loadImages(page);
                        Log.d(TAG, "onLoadMore.END");
                    }
                };

                recyclerView_images.addOnScrollListener(recyclerViewScrollListener);
            }

            recyclerView_images.setAdapter(mImageRecyclerViewAdapter);
        }

        registerNetworkStateChangedListener();
        loadImages(mPage);


        Log.d(TAG, "onCreate.End");

    } // end onCreate()

    public void loadImages(int page) {

        Log.d(TAG, "loadImages.start");
        Log.d(TAG, "loadImages page=" + page);
        if (mRestInterface != null && Utils.isOnline(this) && page > 0) {

          //  mProgressBarHelper.showProgressBar();
            Call call = mRestInterface.imageList("" + page, "" + mLimit);

            ImageListRetrofitCallback imageListRetrofitCallback = new ImageListRetrofitCallback(DashboardActivity.this);

            call.enqueue(imageListRetrofitCallback);
        } else {
            Utils.alertDialogShow(this, getApplicationContext().getString(R.string.no_internet));
        }
        Log.d(TAG, "loadImages.end");
    } // end loadImages()


    private static class ImageListRetrofitCallback implements Callback {

        WeakReference<DashboardActivity> weakReference;

        DashboardActivity dashboardActivity;

        public ImageListRetrofitCallback(DashboardActivity dashboardActivity) {

            Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback.start");
            this.weakReference = new WeakReference<>(dashboardActivity);
            this.dashboardActivity = weakReference.get();
            Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback.end");
        } // end ImageListRetrofitCallback()


        @Override
        public void onResponse(Call call, Response response) {
            if (dashboardActivity != null) {
                Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback onResponse.start");
                Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback response=" + response);
                if (response != null) {
                    //dashboardActivity.mProgressBarHelper.hideProgressBar();
                    Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback response.body()=" + response.body());
                    Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback response.toString()=" + response.toString());
                    if (response.isSuccessful()) {
                        ArrayList<Image> imageList = (ArrayList<Image>) response.body();
                        if (imageList != null && imageList.size() > 0) {

                            dashboardActivity.mImageListResponse.addAll(imageList);
                            if (dashboardActivity.mImageListResponse != null && dashboardActivity.mImageRecyclerViewAdapter != null) {
                                dashboardActivity.mImageRecyclerViewAdapter.addImageList(dashboardActivity.mImageListResponse);
                            }

                            Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback  dashboardActivity.mImageListResponse=" + dashboardActivity.mImageListResponse);
                        }

                    }


                }


                Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback onResponse.End");
            }


        } // end onResponse()

        @Override
        public void onFailure(Call call, Throwable t) {
            Log.d("B=B", "ImageListRetrofitCallback onFailure t=" + t);
            if (dashboardActivity != null) {
              //  dashboardActivity.mProgressBarHelper.hideProgressBar();
              //  Utils.alertDialogShow(dashboardActivity, "" + t);

                {
                    Utils.alertDialogShow(dashboardActivity, dashboardActivity.getApplicationContext().getString(R.string.no_internet));
                }



                Log.d(dashboardActivity.TAG, "ImageListRetrofitCallback onFailure.end");

            }

        } // end onFailure()
    } // end class ImageListRetrofitCallback


    public class ImageRecyclerViewAdapter extends RecyclerView.Adapter<ImageRecyclerViewAdapter.ViewHolder> {

        String SUB_TAG = TAG + " ImageRecyclerViewAdapter";
        ArrayList<Image> imageList;

        Context context;


        public ImageRecyclerViewAdapter(Context context) {
            super();
            Log.d(SUB_TAG, "ImageRecyclerViewAdapter context=" + context);
            this.context = context;
            imageList = new ArrayList<Image>();
        } // end ImageRecyclerViewAdapter()

        @Override
        public int getItemViewType(int position) {
            Log.d(SUB_TAG, "getItemViewType position=" + position);
            return super.getItemViewType(position);
        } // end getItemViewType()

        public void addImageList(ArrayList<Image> imageList) {

            Log.d(SUB_TAG, "addImageList imageList=" + imageList);
            if (this.imageList == null) {
                this.imageList = new ArrayList<Image>();
            }
            if (this.imageList != null && imageList != null) {
                this.imageList = imageList;
                this.notifyDataSetChanged();
            }
        } // end addImageList()

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            Log.d(SUB_TAG, "onCreateViewHolder.start");
            Log.d(SUB_TAG, "onCreateViewHolder position=" + i);
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.image_item_layout, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(v);
            Log.d(SUB_TAG, "onCreateViewHolder.end");
            return viewHolder;
        } // end onCreateViewHolder()

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            Log.d(SUB_TAG, "onBindViewHolder.start");
            Log.d(SUB_TAG, "onBindViewHolder position=" + position);
            Image image = (Image) imageList.get(position);

            if (image != null && viewHolder != null) {

                viewHolder.imageView_thumbnail.setTag("" + image.getId());
                viewHolder.imageView_thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(SUB_TAG, "onBindViewHolder imageView_item onClick");
                        Intent intent = new Intent();
                        intent.setClass(DashboardActivity.this, FullScreenImageActivity.class);
                        intent.putExtra(Utils.SELECTED_IMAGE_KEY, new Gson().toJson(image));
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter,0);
                        changeNavigationBarColor();

                    }
                });
                viewHolder.textView_author_name.setText(image.getAuthor());


                loadImageByGlide(getApplicationContext(), viewHolder.imageView_thumbnail, viewHolder.progressBar_image, image.getThumbnailImageUrl());
                try {
                    viewHolder.imageView_thumbnail.setAspectRatio(image.getAspectRatio());
                } catch (Exception e) {
                    Log.d(SUB_TAG, "onBindViewHolder imageView_thumbnail.setAspectRatio Exception"+e.getMessage());
                    e.printStackTrace();
                }
                // this method is used for prevent messed the item view
                viewHolder.setIsRecyclable(false);

            }
            Log.d(SUB_TAG, "onBindViewHolder.end");
        } // end onBindViewHolder()


        @Override
        public long getItemId(int position) {
            Log.d(SUB_TAG, "getItemId position=" + position);
            return super.getItemId(position);
        }

        @Override
        public int getItemCount() {
            Log.d(SUB_TAG, "getItemCount =" + imageList.size());
            return imageList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            com.chaitanyadubey.gelatogallery.view.DynamicHeightNetworkImageView imageView_thumbnail;
            ProgressBar progressBar_image;
            TextView textView_author_name;

            public ViewHolder(View itemView) {
                super(itemView);
                Log.d(SUB_TAG, "ViewHolder");
                imageView_thumbnail = (com.chaitanyadubey.gelatogallery.view.DynamicHeightNetworkImageView) itemView.findViewById(R.id.imageView_thumbnail);
                progressBar_image = (ProgressBar) itemView.findViewById(R.id.progressBar_image);
                textView_author_name = (TextView) itemView.findViewById(R.id.textView_author_name);
            }


        } // end class ViewHolder

        public  void loadImageByGlide(Context context, final ImageView imageView, final ProgressBar progressBar, String imageURL) {

            Log.d(TAG,"loadImageByGlide onLoadFailed GlideException imageURL="+imageURL);
            try {
                Glide
                        .with(context)
                        .load(imageURL)
                        .centerCrop()
                        .apply(
                                new RequestOptions()
                                        .error(R.drawable.image_place_holder)
                                        .centerCrop()
                        )
                        .placeholder(R.drawable.image_place_holder)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                imageView.setImageResource(R.drawable.image_place_holder);
                                Log.d(TAG,"loadImageByGlide onLoadFailed GlideException ="+e.getMessage());

                                if (progressBar != null) {
                                    progressBar.setVisibility(View.GONE);
                                }
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                //on load success
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(imageView);
            } catch (Exception e) {
                Log.d(TAG,"loadImageByGlide Exception="+e.getMessage());
                e.printStackTrace();
            }


        }


    } // end class ImageRecyclerViewAdapter

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult.start");

        Log.d(TAG, "onActivityResult requestCode=" + requestCode);
        Log.d(TAG, "onActivityResult resultCode=" + resultCode);
        Log.d(TAG, "onActivityResult data=" + data);

        if (requestCode == Utils.FULL_SCREEN_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    int selectedPosition = data.getIntExtra(Utils.SELECTED_IMAGE_KEY, 0);

                    Log.d(TAG, "onActivityResult selectedPosition=" + selectedPosition);

                    if (mStaggeredGridLayoutManager != null && selectedPosition > 0) {
                        mStaggeredGridLayoutManager.scrollToPosition(selectedPosition);
                    }

                }
            }

        }
    } // end onActivityResult()


    @SuppressLint("UseCompatLoadingForDrawables")
    private void loadSideNavigation() {

        Log.d(TAG, "loadSideNavigation");
        mDrawerLayoutDashboard=(DrawerLayout)findViewById(R.id.drawerLayout_dashboard_activity_root);

        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayoutDashboard,  R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayoutDashboard.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        if ( getSupportActionBar()!=null) {
            // to show hamburger icon and back arrow
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mActionBarDrawerToggle.syncState();


        mNavigationViewDashboard = (NavigationView)findViewById(R.id.navigationView_dashboard);
        mNavigationViewDashboard.setItemIconTintList(null);

        MenuCompat.setGroupDividerEnabled(mNavigationViewDashboard.getMenu(), true);
        mNavigationViewDashboard.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                menuEvent(item);

                mDrawerLayoutDashboard.closeDrawer(Gravity.LEFT);
                return true;

            }

        });

        View header = mNavigationViewDashboard.getHeaderView(0);


//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Log.d(TAG, "loadSideNavigation end");
    }

    public void menuEvent(MenuItem item) {

        Log.d(TAG, "menuEvent start");

        int itemID=item.getItemId();


        Log.d(TAG, "menuEvent end");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG,"onConfigurationChanged");

        if ( getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
            mSpanCount = 4;
        } else {
            mSpanCount = 2;
        }
        if (mStaggeredGridLayoutManager!=null) {
            mStaggeredGridLayoutManager.setSpanCount(mSpanCount);
        }


    }


    private BroadcastReceiver mNetworkStateChangedListener;
    protected void registerNetworkStateChangedListener()
    {
        Log.d(TAG,"registerNetworkStateChangedListener.START");

        mNetworkStateChangedListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "mNetworkStateChangedListener.onReceive.START");
                boolean isOnline=Utils.isOnline(context);
                onNetworkStateChanged(isOnline);
                Log.d(TAG, "mNetworkStateChangedListener.onReceive.END");
            }
        };

        Log.d(TAG,"registerNetworkStateChangedListener.END");

        registerReceiver(mNetworkStateChangedListener,
                new IntentFilter(Utils.NETWORK_STATE_CHANGED_INTENT_ACTION));
    }



    private void unregisterNetworkStateChangedListener() {
        Log.d(TAG, "unregisterNetworkStateChangedListener.START");
        if (mNetworkStateChangedListener !=null)
        {
            try {
                unregisterReceiver(mNetworkStateChangedListener);
                mNetworkStateChangedListener =null;
                Log.d(TAG,"unregisterNetworkStateChangedListener.unregistered");
            } catch (Exception e) {
                mNetworkStateChangedListener =null;
                Log.d(TAG,"unregisterNetworkStateChangedListener.Exception="+e.toString());
            }
        }
        Log.d(TAG, "unregisterNetworkStateChangedListener.END");
    }

    protected void onNetworkStateChanged(boolean isOnline)
    {
        Log.d(TAG,"onNetworkStateChanged.isOnline="+isOnline);

        if (Utils.isOnline(getApplicationContext())&& mPage==1)
        {
            loadImages(mPage);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterNetworkStateChangedListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        restoreNavigationColor();
    }

    private void restoreNavigationColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(Color.parseColor(mNavigationBarColor));
        }else{
            getWindow().setNavigationBarColor(Color.parseColor(mNavigationBarColor));
        }
    }

    private void changeNavigationBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.black));
        }else{
            getWindow().setNavigationBarColor(ContextCompat.getColor(DashboardActivity.this, R.color.black));
        }
    }
} // class DashboardActivity

