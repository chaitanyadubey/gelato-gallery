package com.chaitanyadubey.gelatogallery.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.chaitanyadubey.gelatogallery.R;
import com.chaitanyadubey.gelatogallery.entity.Image;
import com.chaitanyadubey.gelatogallery.helper.Utils;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.Intent.EXTRA_CHOSEN_COMPONENT;

public class FullScreenImageActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "B=B" + getClass();
    com.github.chrisbanes.photoview.PhotoView imageView_full_screen;
    ProgressBar progressBar_image;
    Image mImage;
    ImageView imageView_refresh;
    SocialMediaShareReceiver mSocialMediaShareReceiver;
    private PendingIntent mPendingIntentSocialMediaShareReceiver;
    final String shareAction = "com.chaitanyadubey.gelatogallery.SHARE_ACTION";
    String mSocialMediaShareType = "";

    public static final int PERMISSION_READ_EXTERNAL_STORAGE = 1;
    public static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 2;
    private List<String> ignoredPermissionsArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate.start");
        super.onCreate(savedInstanceState);
        changeColor();
        setContentView(R.layout.full_screen_image_activity_layout);
        Intent intent = getIntent();

        String imageJsonString = intent.getStringExtra(Utils.SELECTED_IMAGE_KEY);
        Log.d(TAG, "onCreate imageJsonString=" + imageJsonString);
        if (!Utils.isEmptyOrNull(imageJsonString)) {
            mImage = Utils.getGsonParser().fromJson(imageJsonString, Image.class);
        }
        Log.d(TAG, "onCreate mImage=" + mImage);
        imageView_full_screen = findViewById(R.id.imageView_full_screen);

        imageView_refresh= findViewById(R.id.imageView_refresh);
        progressBar_image = (ProgressBar) findViewById(R.id.progressBar_image);

        if (mImage != null && imageView_full_screen != null) {
            if (getSupportActionBar() != null) {
                if (!Utils.isEmptyOrNull(mImage.getAuthor())) {
                    getSupportActionBar().setTitle(mImage.getAuthor());
                }
            }

            loadImageByGlide();

        }

        if (imageView_refresh!=null) {
            imageView_refresh.setVisibility(View.GONE);
            imageView_refresh.setOnClickListener(this);
        }


        registerReceiver(onDownLoadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        Intent receiver = new Intent(shareAction);
        mPendingIntentSocialMediaShareReceiver = PendingIntent.getBroadcast(getApplicationContext(), 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT);
        this.registerReceiver(mSocialMediaShareReceiver, new IntentFilter(shareAction));

        Log.d(TAG, "onCreate.end");

    }


    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        Log.d(TAG, "onClick viewId=" + viewId);


        if (imageView_refresh!=null  && viewId==imageView_refresh.getId()) {
            imageView_refresh.setVisibility(View.GONE);
            if (progressBar_image!=null) {
                progressBar_image.setVisibility(View.VISIBLE);
            }
            loadImageByGlide();
        }

    }


    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed=");
        finish();
        overridePendingTransition(R.anim.fadein,R.anim.fadeout);
        super.onBackPressed();
    }


    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.d(TAG, "onCreateOptionsMenu.start");

        Log.d(TAG, "onCreateOptionsMenu menu" + menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.image_menu, menu);

        if (menu instanceof MenuBuilder) {
            MenuBuilder menuBuilder = (MenuBuilder) menu;
            menuBuilder.setOptionalIconsVisible(true);
        }

        Log.d(TAG, "onCreateOptionsMenu.end");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.d(TAG, "onOptionsItemSelected.start");
        Log.d(TAG, "onOptionsItemSelected item=" + item);
        int id = item.getItemId();


        switch (id) {
            case android.R.id.home:

                Log.d(TAG, "onOptionsItemSelected MenuItem  onClick home");
                onBackPressed();
                return true;

            case R.id.download:
                Log.d(TAG, "onOptionsItemSelected MenuItem onClick download");
                if (hasPermission(this, this, PERMISSION_WRITE_EXTERNAL_STORAGE) == true &&
                        hasPermission(this, this, PERMISSION_READ_EXTERNAL_STORAGE) == true) {
                    if (mImage != null && !Utils.isEmptyOrNull(mImage.getDownload_url())) {
                        if (Utils.isOnline(this)) {
                            downloadImageNew("gelato-" + (new Date().getTime()), mImage.getDownload_url());
                        } else {
                            Utils.alertDialogShow(this, getApplicationContext().getString(R.string.no_internet));
                        }

                    }
                }
                return true;
            case R.id.share:
                Log.d(TAG, "onOptionsItemSelected MenuItem  onClick share");
                if (hasPermission(this, this, PERMISSION_WRITE_EXTERNAL_STORAGE) == true &&
                        hasPermission(this, this, PERMISSION_READ_EXTERNAL_STORAGE) == true) {
                    if (imageView_full_screen != null && imageView_full_screen.getDrawable() != null) {
                        Bitmap bitmap = ((BitmapDrawable) imageView_full_screen.getDrawable()).getBitmap();
                        if (bitmap != null) {
                            Uri uri = getImageUri(getApplicationContext(), bitmap);
                            if (uri != null) {
                                shareImage(uri);
                            }
                        }
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void downloadImageNew(String filename, String downloadUrlOfImage) {

        Log.d(TAG, "downloadImageNew.start");
        try {
            mImageFileName = filename + ".jpg";
            DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(downloadUrlOfImage);
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setTitle("Downloading " + filename)
                    .setDescription("Downloading " + filename)
                    .setMimeType("image/jpeg") // Your file type. You can use this code to download other file types also.
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    .setVisibleInDownloadsUi(true)
                    .setAllowedOverRoaming(true)
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, File.separator + mImageFileName);
            downloadManager.enqueue(request);
            Toast.makeText(this, getApplicationContext().getString(R.string.download_started), Toast.LENGTH_SHORT).show();
//            Utils.alertDialogShow(this, getApplicationContext().getString(R.string.download_started));
        } catch (Exception e) {
//            Toast.makeText(this, getApplicationContext().getString(R.string.download_failed), Toast.LENGTH_SHORT).show();
            Log.d(TAG, "downloadImageNew Exception=" + e.getMessage());
            Utils.alertDialogShow(this, getApplicationContext().getString(R.string.download_failed));
        }

        Log.d(TAG, "downloadImageNew.end");
    }

    String mImageFileName = "";
    //    This BroadcastReceiver is used for updated downLoading complete by Android downLoad manager
    BroadcastReceiver onDownLoadComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            Log.d(TAG, "onDownLoadComplete onReceive.start");

            try {
                String downloadCompleteMessage = getApplicationContext().getString(R.string.image_downloaded);
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), mImageFileName);
                if (file != null) {
                    downloadCompleteMessage += "\n \n" + file.toString();
                }
                //openImageFolder();
                Utils.alertDialogShow(FullScreenImageActivity.this, downloadCompleteMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onDownLoadComplete onReceive.end");
        }
    };
    // end onDownLoadComplete

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy.start");
        try {
            unregisterReceiver(onDownLoadComplete);
            unregisterReceiver(mSocialMediaShareReceiver);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "onDestroy Exception=" + e.getMessage());
        }

        super.onDestroy();
        Log.d(TAG, "onDestroy.end");
    }

    private Uri getImageUri(Context context, Bitmap bitmap) {
        Log.d(TAG, "getImageUri.start");
        String path = "";
        Uri uri=null;
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Gelato", null);
            if (!Utils.isEmptyOrNull(path)) {
                uri=Uri.parse(path);
            }
        } catch (Exception e) {
            Log.d(TAG,"getImageUri Exception="+e.getMessage());
            e.printStackTrace();
        }
        Log.d(TAG, "getImageUri.end");
        return uri;
    }


    @SuppressLint("NewApi")
    private void shareImage(Uri imageUri) {
        Log.d(TAG, "shareImage.START");
        if (imageUri != null) {

            String app_name = getApplicationContext().getResources().getString(R.string.app_name);

            String author = "";
            if (mImage!=null) {
                author = mImage.getAuthor();
            }
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_SUBJECT, "" + app_name + " " + (new Date().getTime()) + " " + author);
            intent.putExtra(Intent.EXTRA_TEXT, "");
            intent.putExtra(Intent.EXTRA_STREAM, imageUri);
            try {
                String packageName = getPackageManager().getPackageInfo(getPackageName(), 0).packageName;
                startActivity(Intent.createChooser(intent
                        , "" + app_name + " " + getApplicationContext().getString(R.string.share)
                        , mPendingIntentSocialMediaShareReceiver.getIntentSender()).addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION));


            } catch (ActivityNotFoundException | PackageManager.NameNotFoundException e) {
                Log.d(TAG, "shareImage.Exception=" + e.getMessage());
                Toast.makeText(FullScreenImageActivity.this, "No App Available", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.d(TAG, "shareImage.Exception=" + e.getMessage());
                Toast.makeText(FullScreenImageActivity.this, "No App Available", Toast.LENGTH_SHORT).show();
            }
        }
        Log.d(TAG, "shareImage.END");
    }

    //  SocialMediaShareReceiver is used for gets info share media of image
    public static class SocialMediaShareReceiver extends BroadcastReceiver {
        private WeakReference<FullScreenImageActivity> activityWeakReference;

        public SocialMediaShareReceiver(FullScreenImageActivity context) {

            activityWeakReference = new WeakReference<>(context);
            FullScreenImageActivity activity = activityWeakReference.get();
            if (activity != null) {
                Log.d(activity.TAG, "SocialMediaShareReceiver");
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            FullScreenImageActivity activity = activityWeakReference.get();
            if (activity != null) {
                String selectedAppPackage = String.valueOf(intent.getExtras().get(EXTRA_CHOSEN_COMPONENT));

                Log.d(activity.TAG, "SocialMediaShareReceiver onReceive selectedAppPackage=" + selectedAppPackage);
                //  activity.appsFlyerShareWorkoutData(activity.memberID,"");


                if (!Utils.isEmptyOrNull(selectedAppPackage)) {
                    try {
                        if (Utils.containsIgnoreCase(selectedAppPackage, "ComponentInfo{")) {
                            selectedAppPackage = selectedAppPackage.replace("ComponentInfo{", "");
                            selectedAppPackage = selectedAppPackage.replace("}", "");
                        }
                        activity.mSocialMediaShareType = "" + selectedAppPackage;

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d(activity.TAG, "SocialMediaShareReceiver Exception=" + e.getMessage());
                    }
                    Log.d(activity.TAG, "SocialMediaShareReceiver onReceive socialMediaType=" + activity.mSocialMediaShareType);
                }
            }
        }
    }    // end class SocialMediaShareReceiver

    private void checkPermissionRationale(String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            Log.d(TAG, "shouldShowRequestPermissionRationale START");
//            showPermissionReasonDialog(permission);
            Utils.setOnPermissionDialogClickListener(new Utils.OnPermissionDialogClickListener() {
                @Override
                public void onDialogDismiss() {
                    checkPermissions();
                }
            });
            Utils.showPermissionReasonDialog(permission, FullScreenImageActivity.this);
            Log.d(TAG, "shouldShowRequestPermissionRationale END");
        } else {
            if (ignoredPermissionsArray == null) ignoredPermissionsArray = new ArrayList<>();
            if (!ignoredPermissionsArray.contains(permission)) {
                ignoredPermissionsArray.add(permission);
            }
            Log.d(TAG, "NOT shouldShowRequestPermissionRationale END");
            checkPermissions();
        }
    }

    public void checkPermissions() {
        Log.d(TAG, "checkSelfPermission START");
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Log.d(TAG, "checkSelfPermission Android OS > M");


                if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && !ignoredPermissionsArray.contains(READ_EXTERNAL_STORAGE)) {
                    Log.d(TAG, "checkSelfPermission READ_EXTERNAL_STORAGE is TRUE");
                    ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE}, PERMISSION_READ_EXTERNAL_STORAGE);
                } else {
                    Log.d(TAG, "checkSelfPermission READ_EXTERNAL_STORAGE is FALSE");
                    if (ActivityCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && !ignoredPermissionsArray.contains(WRITE_EXTERNAL_STORAGE)) {
                        Log.d(TAG, "checkSelfPermission WRITE_EXTERNAL_STORAGE is TRUE");
                        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_EXTERNAL_STORAGE);
                    } else {
                        Log.d(TAG, "checkSelfPermission WRITE_EXTERNAL_STORAGE is FALSE");
                    }
                }

            }

            Log.d(TAG, "checkSelfPermission END");

        } catch (Exception e) {
            Log.e(TAG, "hasPermission.Exception e" + e.getMessage());
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        Log.d(TAG, "onRequestPermissionsResult.START");
        Log.d(TAG, "requestCode=" + requestCode);

        int grant = PackageManager.PERMISSION_DENIED;

        if (grantResults != null && grantResults.length > 0) {
            grant = grantResults[0];
        }

        Log.d(TAG, "grant=" + grant);
        switch (requestCode) {


            case PERMISSION_WRITE_EXTERNAL_STORAGE:
                if (grant == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "PERMISSION_WRITE_EXTERNAL_STORAGE granted");
                    if (!hasAllPermissions(this, Utils.PERMISSIONS)) {
                        checkPermissions();
                    }
                } else {
                    if (!ignoredPermissionsArray.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE) && !ignoredPermissionsArray.contains(READ_EXTERNAL_STORAGE)) {
                        checkPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                }
                break;

            case PERMISSION_READ_EXTERNAL_STORAGE:
                if (grant == PackageManager.PERMISSION_GRANTED) {
                    if (!hasAllPermissions(this, Utils.PERMISSIONS)) {
                        checkPermissions();
                    }
                    Log.d(TAG, "PERMISSION_READ_EXTERNAL_STORAGE granted");
                } else {
                    if (!ignoredPermissionsArray.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE) && !ignoredPermissionsArray.contains(READ_EXTERNAL_STORAGE)) {
                        checkPermissionRationale(READ_EXTERNAL_STORAGE);
                    }
                }
                break;

            default:
                finish();
                overridePendingTransition(0,R.anim.fadeout);
                break;
        }

        Log.d(TAG, "onRequestPermissionsResult.END");
    }

    public static boolean hasAllPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean hasPermission(Context context, Activity activity, int permissionCode) {

        boolean hasPermission = true;

        Log.d(TAG, "hasPermission.START permissionCode=" + permissionCode);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int permissionCheck = 0;


                if (permissionCode == PERMISSION_READ_EXTERNAL_STORAGE) {
                    permissionCheck = context.checkCallingOrSelfPermission(READ_EXTERNAL_STORAGE);
                    Log.d(TAG, "PERMISSION_READ_EXTERNAL_STORAGE permissionCheck=" + permissionCheck);
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        hasPermission = false;
                        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE}, PERMISSION_READ_EXTERNAL_STORAGE);
                    }
                }

                if (permissionCode == PERMISSION_WRITE_EXTERNAL_STORAGE) {
                    permissionCheck = context.checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    Log.d(TAG, "PERMISSION_WRITE_EXTERNAL_STORAGE permissionCheck=" + permissionCheck);
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        hasPermission = false;

                        Log.d(TAG, "Request permission for write external");

                        boolean shouldShowRequestPermissionRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        Log.d(TAG, "shouldShowRequestPermissionRationale=" + shouldShowRequestPermissionRationale);

                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_EXTERNAL_STORAGE);
                    }
                }


            } else {
                Log.d(TAG, "hasPermission: No need to check permissions. SDK version < LOLLIPOP.");
            }
        } catch (Exception e) {
            Log.e(TAG, "hasPermission.Exception e" + e.getMessage());
        }

        return hasPermission;
    }

    private void changeColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.black));
        }else{
            getWindow().setNavigationBarColor(ContextCompat.getColor(FullScreenImageActivity.this, R.color.black));
        }
    }

    public  void loadImageByGlide() {
        try {
            Glide
                    .with(this)
                    .load(mImage.getFullScreenImageUrl())
                    .apply(
                            new RequestOptions()
                                    .error(R.drawable.image_place_holder)

                    )
                    .placeholder(R.drawable.image_place_holder)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            imageView_full_screen.setImageResource(R.drawable.image_place_holder);
                            Log.d(TAG,"onLoadFailed GlideException="+e.getMessage());
                           if(!Utils.isOnline(getApplicationContext()))
                           {
                               Utils.alertDialogShow(FullScreenImageActivity.this, getApplicationContext().getString(R.string.no_internet));
                           }

                            if (progressBar_image != null) {
                                progressBar_image.setVisibility(View.GONE);
                            }
                            Log.d(TAG,"onLoadFailed GlideException imageView_refresh="+imageView_refresh);
                            if (imageView_refresh!=null) {
                                imageView_refresh.setVisibility(View.VISIBLE);

                            }

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            //on load success
                            Log.d(TAG,"onResourceReady onResourceReady");
                            if (progressBar_image!=null) {
                                progressBar_image.setVisibility(View.GONE);
                            }
                            if (imageView_refresh!=null) {
                                imageView_refresh.setVisibility(View.GONE);

                            }

                            return false;
                        }
                    })
                    .into(imageView_full_screen);
        } catch (Exception e) {
            Log.d(TAG, "loadImage Exception=" + e.getMessage());
            e.printStackTrace();
        }
    }

}