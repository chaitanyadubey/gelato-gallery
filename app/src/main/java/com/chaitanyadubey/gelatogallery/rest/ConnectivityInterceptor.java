package com.chaitanyadubey.gelatogallery.rest;

/*
    This class is used as an Interceptor during API communication
 */

import android.content.Context;
import android.util.Log;


import com.chaitanyadubey.gelatogallery.helper.Utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by DELL on 2/9/2018.
 */

public class ConnectivityInterceptor implements Interceptor {

    String TAG = "B=B " + getClass();

    private Context mContext;


    public ConnectivityInterceptor(Context context) {
        mContext = context;
        init();
    }

    private void init() {
        Log.d(TAG, "intercept.init.START");


        Log.d(TAG, "intercept.init.END");

    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Log.d(TAG, "intercept.START");

        init();

        try {
            if (!Utils.isOnline(mContext)) {
                throw new ConnectivityException();
            }
        } catch (ConnectivityException e) {
            e.printStackTrace();
        }

        Request.Builder builder = chain.request().newBuilder();

        Request request = builder
//                addHeader(Headers)

                .build();

        Log.d(TAG, "intercept request=" + request);


        return chain.proceed(request);
    }

}
