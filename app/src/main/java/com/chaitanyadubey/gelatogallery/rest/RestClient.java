package com.chaitanyadubey.gelatogallery.rest;

/**
 * Created by Chaitanya on 6/10/2016.
 * This class is responsible for providing API communication
 */

import android.content.Context;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
     public static final String API_URL = "https://picsum.photos/";



    private RestInterface mApiService;
    private OkHttpClient mClient;

    public RestClient(Context context) {

        mClient = new OkHttpClient.Builder()
                .addInterceptor(new ConnectivityInterceptor(context))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .client(mClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApiService = retrofit.create(RestInterface.class);
    }

    public RestInterface getApiService() {
        return mApiService;
    }

    public OkHttpClient getClient() {
        return mClient;
    }

}
