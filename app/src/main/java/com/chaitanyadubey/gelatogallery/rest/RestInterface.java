package com.chaitanyadubey.gelatogallery.rest;

import com.chaitanyadubey.gelatogallery.entity.Image;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Chaitanya on Feb 16, 2021
 *
 * This class provides the API signature to make the api call wherever needed
 * This is the feature of Retrofit technology.
 */




public interface RestInterface {


    @GET("v2/list")
    Call<ArrayList<Image>> imageList(@Query("page") String page,
                                     @Query("limit")  String limit);

}