package com.chaitanyadubey.gelatogallery.rest;

import java.io.IOException;

/**
 *  This class helps handling the Internet Connectivity Exceptions
 *  during the API communication.
 *
 */

public class ConnectivityException  extends IOException {


    @Override
    public String getMessage()
    {
        return "Please check your internet connection.";
    }

}
