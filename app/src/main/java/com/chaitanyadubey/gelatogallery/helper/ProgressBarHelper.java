package com.chaitanyadubey.gelatogallery.helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.chaitanyadubey.gelatogallery.R;

import java.lang.ref.WeakReference;

public class ProgressBarHelper {
    public static final String TAG = "B=B ProgressBarHelper";

    private WeakReference<ProgressBar> mProgressBarWeakReference;
    private WeakReference<Activity> mActivityWeakReference;

    private boolean isModal = true; // default value of modal
    private boolean isNotDismissible;

    public ProgressBarHelper(Activity activity,boolean isModal,boolean isNotDismissible) {
        this.isModal = isModal;
        this.isNotDismissible = isNotDismissible;
        initReferences(activity);
    }

    // The modal flag makes the everything else untouchable / un clickable.
    public ProgressBarHelper(Activity activity,boolean isModal) {
        this.isModal = isModal;

        initReferences(activity);
    }

    //Adds a progress bar in the center of the screen
    public ProgressBarHelper(Activity activity) {


        Log.d(TAG, "ProgressBarHelper.constructor()");

        initReferences(activity);
    }

    public void initReferences(Activity activity)
    {
        Log.d(TAG, "initReferences.START()");

        ViewGroup layout = (ViewGroup) ((Activity) activity).findViewById(android.R.id.content).getRootView();

        ProgressBar progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyleLarge);


        RelativeLayout.LayoutParams paramsProgressBar = new
                RelativeLayout.LayoutParams(96, 96);
        progressBar.setLayoutParams(paramsProgressBar);
        progressBar.setIndeterminateDrawable(layout.getResources().getDrawable(R.drawable.progressbar_animation));
        progressBar.setIndeterminate(true);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);




        RelativeLayout rl = new RelativeLayout(activity);

        rl.setGravity(Gravity.CENTER);
        rl.addView(progressBar);

        layout.addView(rl, params);

        // Keeping the weak reference of Activity and progressbar
        mActivityWeakReference = new WeakReference<>(activity);
        mProgressBarWeakReference = new WeakReference<ProgressBar>(progressBar);

        hideProgressBar();
    }

    public void hideKeyboard() {

        Log.d(TAG,"hideKeyboard()");

        View view = null;

        if(mActivityWeakReference!=null) {
            Activity activity = mActivityWeakReference.get();

            if (activity != null) {
                view = activity.getCurrentFocus();
            }

            if (view != null) {

                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showProgressBar()
    {
        Log.d(TAG, "showProgressBar()");

        if(mProgressBarWeakReference !=null) {

            ProgressBar progressBar = mProgressBarWeakReference.get();

            if (progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        hideKeyboard();

        if (mActivityWeakReference != null) {
            Activity activity = mActivityWeakReference.get();

            if (activity != null) {
                if (isModal) {
                    Window window = activity.getWindow();
                    if (window!=null) {
                        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                }
            }
        }

    }
    public void hideProgressBar()
    {
        Log.d(TAG,"hideProgressBar()");

        if(mProgressBarWeakReference!=null) {
            ProgressBar progressBar = mProgressBarWeakReference.get();

            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }

        if(mActivityWeakReference!=null) {
            Activity activity = mActivityWeakReference.get();

            if (activity != null) {
                if (!isNotDismissible) {
                    Window window = activity.getWindow();
                    if (window!=null) {
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                }
            }
        }
    }

    public boolean isShowing(){
        Log.d(TAG,"isShowing.START");
        boolean isVisible=false;
        if(mProgressBarWeakReference !=null) {

            ProgressBar progressBar = mProgressBarWeakReference.get();

            if (progressBar != null) {
                if (progressBar.getVisibility()==View.VISIBLE){
                    isVisible = true;
                    Log.d(TAG,"isShowing.isVisible="+isVisible);
                    return isVisible;
                }
            }
        }

        Log.d(TAG,"isShowing.isVisible="+isVisible);
        return isVisible;
    }
    public void showKeyboard(View view){
        Log.d(TAG,"showKeyboard.START");
        if (mActivityWeakReference!=null) {
            Activity activity = mActivityWeakReference.get();
            try {
                if (activity!=null) {
                    view.requestFocus();
                    InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG,"showKeyboard.END");
    }
}
