package com.chaitanyadubey.gelatogallery.helper;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.UiModeManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import android.widget.TextView;


import androidx.core.content.ContextCompat;



import com.chaitanyadubey.gelatogallery.R;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static android.content.Context.UI_MODE_SERVICE;

public class Utils {

    public static String TAG = "B=B Utils";


    public static String SELECTED_IMAGE_KEY = "SELECTED_IMAGE_KEY";


    public static final String NETWORK_STATE_CHANGED_INTENT_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    public static int FULL_SCREEN_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    public static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    public static boolean isOnline(Context context) {

        Log.d(TAG, "isOnline");
        boolean isOnline = false;

        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();

            isOnline = (netInfo != null && netInfo.isConnected());
        }

        Log.d(TAG, "isOnline=" + isOnline);


        return isOnline;
    }


    public static void alertDialogShow(final Context context, String message) {
        try {

            Log.d(TAG, "alertDialogShow message=" + message);

            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.alert_dialog_layout, null);
            alertDialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = alertDialogBuilder.create();

            TextView textView_alert_dialog_title = (TextView) dialogView.findViewById(R.id.textView_alert_dialog_title);
            textView_alert_dialog_title.setText(context.getResources().getString(R.string.app_name));
            TextView textView_alert_dialog_message = (TextView) dialogView.findViewById(R.id.textView_alert_dialog_message);
            // textView_alert_dialog_message.setText(message);

            if (textView_alert_dialog_message != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    textView_alert_dialog_message.setText(Html.fromHtml(message, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    textView_alert_dialog_message.setText(Html.fromHtml(message));
                }
            }


            Button button_ok = (Button) dialogView.findViewById(R.id.button_ok);

            //MODAL
            alertDialog.setCancelable(false);
            try {
                Window window = alertDialog.getWindow();
                if (window != null) {
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            button_ok.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();

                }

            });

            alertDialog.show();
        } catch (Exception e) {
            Log.d(TAG, "alertDialogShow.Exception e=" + e.getMessage());
        }
    }


    public static Gson getGsonParser() {


        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        return gson;
    }


    public static boolean isEmptyOrNull(String str) {
        boolean isEmptyOrNull = false;

        if (str == null || str.trim().length() == 0) {
            isEmptyOrNull = true;
        }

        return isEmptyOrNull;
    }


    public static boolean isTablet(Context context) {
        if (getUserInfoDeviceType(context).equals("tablet")) {
            return true;
        } else {
            return false;
        }

    }

    public static String getUserInfoDeviceType(Context context) {
        String userInfoDeviceType = "mobile";

        boolean isTablet = context.getResources().getBoolean(R.bool.isTablet);

        if (isTablet) {
            userInfoDeviceType = "tablet";
        }

        UiModeManager uiModeManager = (UiModeManager) context.getSystemService(UI_MODE_SERVICE);
        if (uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION) {
            userInfoDeviceType = "television";
        }

        return userInfoDeviceType;

    }

    public static int getIntFromString(String str, int defaultValue) {
        int i = defaultValue;

        try {
            i = Integer.parseInt(str.trim());
        } catch (Exception e) {
            Log.d(TAG, "getIntFromString exception e=" + e.getMessage());
        }
        return i;
    }

    public static boolean containsIgnoreCase(String parent, String child) {
        boolean contains = false;

        if (parent != null && child != null) {
            parent = parent.toLowerCase().trim();
            child = child.toLowerCase().trim();

            if (parent.indexOf(child) != -1) {
                contains = true;
            }
        }
        return contains;
    }


    private static OnPermissionDialogClickListener onPermissionDialogClickListener;
    public static void setOnPermissionDialogClickListener(OnPermissionDialogClickListener onPermissionDialogClickListener) {
        Utils.onPermissionDialogClickListener = onPermissionDialogClickListener;
    }

    public interface OnPermissionDialogClickListener {
        void onDialogDismiss();
    }

    public static void showPermissionReasonDialog(final String permission, Activity activity) {

        Context context = activity;
        String permissionTitle = null;
        String permissionHeadline = null;
        int permissionRequestCode = 0;

        androidx.appcompat.app.AlertDialog alertDialog = null;
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = activity.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_permission_alert_dialog, null);

        TextView buttonOk = (TextView) dialogView.findViewById(R.id.b_ok);

        TextView tvPermissionHeadLine = (TextView) dialogView.findViewById(R.id.tv_permission_headline);
        TextView tvPermissionTitle = (TextView) dialogView.findViewById(R.id.tv_permission_request_title);
        ImageView imgPermissionIcon = (ImageView) dialogView.findViewById(R.id.img_permission_icon);

        String store_permission_msg="In order to store images in the device using Gelato Gallery app, Android requires Storage permissions.";

        if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissionTitle = store_permission_msg;
            permissionHeadline = "Allow " + activity.getResources().getString(R.string.app_name) + " " + activity.getResources().getString(R.string.to_access_read_external_storage);
            imgPermissionIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.permission_storage_icon));
        } else if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissionTitle = store_permission_msg;
            permissionHeadline = "Allow " + activity.getResources().getString(R.string.app_name) + " " + activity.getResources().getString(R.string.to_write_external_storage);
            imgPermissionIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.permission_storage_icon));
        }

        tvPermissionHeadLine.setText(permissionHeadline);
        tvPermissionTitle.setText(permissionTitle);
        //Now we need an AlertDialog.Builder object
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        final androidx.appcompat.app.AlertDialog finalAlertDialog = alertDialog;
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalAlertDialog != null)
                    finalAlertDialog.dismiss();
                if (onPermissionDialogClickListener != null) {
                    onPermissionDialogClickListener.onDialogDismiss();
                }
//                isPermissionGranted();
            }
        });
        alertDialog.show();
    }


}
